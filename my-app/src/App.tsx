import './App.css';
import Login from './components/login/login.component';
import LeafletMap from './components/map/map.component';
import { BrowserRouter, useRoutes } from "react-router-dom";
import SocialWorkerPage from './components/socialworker/socialworker.component';
import Header from './components/header/header.component'
import { Provider } from 'react-redux';
import store from './redux/store/store';
import ActivitiesPage from './components/activityList/activityList.component';



const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <AppWithRoutes />
      </BrowserRouter>
    </Provider>
  );
}

const AppWithRoutes = () => {
  return useRoutes([
    { path: '/', element: <ActivitiesPage /> },
    { path: 'login', element: <Login /> },
    { path: 'worker', element: <SocialWorkerPage /> }
  ]);
}

export default App;
