import { createStore, applyMiddleware, compose } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import rootReducer, { RootState } from '../reducers/rootReducer';

const middleware = [thunk as ThunkMiddleware<RootState>];

const store = createStore(
  rootReducer
);

export default store;