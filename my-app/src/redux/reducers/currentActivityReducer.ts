import { ActivityId } from '../../interfaces/activityInterface';

export interface CurrentActivityAction {
    type: CurrentActivityActionTypes;
    activityId: ActivityId;
}

export enum CurrentActivityActionTypes {
    setCurrentActivity = 'setCurrentActivity'
}

const initialState: ActivityId = null;

const currentActivityReducer = (state = initialState, action: CurrentActivityAction): ActivityId => {
  switch (action.type) {
    case CurrentActivityActionTypes.setCurrentActivity:
        return action.activityId;
    default:
      return state;
  }
};

export default currentActivityReducer;