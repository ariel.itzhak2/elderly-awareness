import { Activity } from "../../interfaces/activityInterface";

export interface AllActivitiesAction {
    type: ActivitiesActionTypes;
    activities: Activity[];
}

export enum ActivitiesActionTypes {
    setActivities = 'setActivities'
}

const initialState: Activity[] = [];

const allActivitiesReducer = (state = initialState, action: AllActivitiesAction): Activity[] => {
  switch (action.type) {
    case ActivitiesActionTypes.setActivities:
        return action.activities;
    default:
      return state;
  }
};

export default allActivitiesReducer;