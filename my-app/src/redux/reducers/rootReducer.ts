import { combineReducers } from 'redux';
import allActivitiesReducer from './allActivitiesReducer';
import currentActivityReducer from './currentActivityReducer';
import currentElderReducer from './currentElderReducer';
import allEldersReducer from './allEldersReducer';
// import your reducers here

const rootReducer = combineReducers({
  currentActivity: currentActivityReducer,
  allActivities: allActivitiesReducer,
  currentElder: currentElderReducer,
  allElders: allEldersReducer
  // add your other reducers here
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;