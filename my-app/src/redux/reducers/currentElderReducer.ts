import { PersonId } from '../../interfaces/personInterface';

export interface CurrentElderAction {
  type: CurrentElderActionTypes;
  personId: PersonId;
}

export enum CurrentElderActionTypes {
  setCurrentElder = 'setCurrentElder'
}

const initialState: PersonId = null;

const currentElderReducer = (state = initialState, action: CurrentElderAction): PersonId => {
  switch (action.type) {
    case CurrentElderActionTypes.setCurrentElder:
      return action.personId;
    default:
      return state;
  }
};

export default currentElderReducer;