import { Elder } from "../../interfaces/elderInterface";

export interface AllEldersAction {
    type: EldersActionTypes;
    elders: Elder[];
}

export enum EldersActionTypes {
    setElders = 'setElders'
}

const initialState: Elder[] = [];

const allEldersReducer = (state = initialState, action: AllEldersAction): Elder[] => {
  switch (action.type) {
    case EldersActionTypes.setElders:
        return action.elders;
    default:
      return state;
  }
};

export default allEldersReducer;