import { List, ListItemButton, ListItemText, Paper, Stack, Typography } from '@mui/material';
import React, { useState } from 'react';
import NavItem from '../../interfaces/navItemInterface'
import './verticalList.component.scss';

interface NavbarProps {
  navItems: NavItem[];
  title?: string;
  titleClassName?: string;
  onItemClick?: (item: NavItem) => void;
  className?: string;
}

const Navbar: React.FC<NavbarProps> = ({ navItems, title, titleClassName, onItemClick, className }) => {

  return (
    <Paper elevation={6} className={className}>
      <Typography
        className={titleClassName}
        variant="body1"
      >
        {title}
      </Typography>
      <List component="nav">
        {navItems.map((item) => (
          <ListItemButton
            onClick={() => onItemClick?.(item)}
            sx={{ width: "100%" }}
          >
            <ListItemText primary={
              <Stack
                direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={0}
              >
                {item.text}
              </Stack>} />
          </ListItemButton>
        ))}
      </List>
    </Paper>
  );
};

export default Navbar;