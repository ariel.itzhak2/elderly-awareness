import React, { useEffect, useMemo, useRef, useState } from 'react';
import Leaflet from 'leaflet';
import NavBar from '../verticalList/verticalList.component';
import NavItem from '../../interfaces/navItemInterface';
import { useDispatch } from 'react-redux';
import { CurrentActivityAction, CurrentActivityActionTypes } from '../../redux/reducers/currentActivityReducer';
import { useSelector } from 'react-redux';
import LeafletMap from '../map/map.component';
import { Activity } from '../../interfaces/activityInterface';
import { ActivitiesActionTypes, AllActivitiesAction } from '../../redux/reducers/allActivitiesReducer';
import { activitiesMock } from '../../mocks/activitiesMock';
import './activityList.component.scss';
import { useNavigate } from 'react-router-dom';
import Header from '../header/header.component';
import { activitiesCommunicator } from '../../communication/activities.communicator';
import { Alert, CircularProgress, Snackbar } from '@mui/material';

const ActivitiesPage = () => {
    const [errorOpen, setErrorOpen] = useState(false);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const navigate = useNavigate();
    console.log(localStorage.getItem("username"));
    if (!localStorage.getItem("username")) {
        navigate("/login");
    }
    const dispatch = useDispatch();
    const allActivities = useSelector((state: { allActivities: Activity[] }) => state.allActivities);
    const activitiesMenuItems: NavItem<Activity>[] = useMemo(() => allActivities.map(activity => {
        return {
            text: activity.title,
            link: activity.title,
            obj: activity
        }
    }), [allActivities]);
    const mapRef = useRef<Leaflet.Map>();

    const setCurrentActivity = (item: NavItem) => {
        const currentActivity = allActivities.find(activity => activity.title === item.link);
        if (currentActivity.location === null) {
            setErrorOpen(false);
            setErrorOpen(true);
            return;
        }
        if (currentActivity && mapRef.current) {
            mapRef.current?.flyTo?.(currentActivity.location, 17, { animate: true, duration: 2 });
        }
        dispatch<CurrentActivityAction>({
            type: CurrentActivityActionTypes.setCurrentActivity,
            activityId: item.link,
        });
    }

    useEffect(() => {
        (async () => {
            const allActivities: Activity[] = await activitiesCommunicator.getAllActivities() ?? activitiesMock;
            setIsLoading(false);
            dispatch<AllActivitiesAction>({
                type: ActivitiesActionTypes.setActivities,
                activities: allActivities,
            });
        })();
    }, []);


    const handleSnackbarClose = () => {
        setErrorOpen(false);
    }

    const url = "https://thumbs.dreamstime.com/b/indonesian-old-man-face-potrait-palembang-city-his-wrinkles-skin-white-hair-indonesia-august-193194107.jpg"

    return (
        <>
            <Header userPhoto={url} />
            <div className='activities-page'>
                {
                    !isLoading ?
                        <>
                            <LeafletMap
                                markers={allActivities}
                                mapRef={mapRef}
                            />
                            <NavBar
                                title='התנדבויות באזורך'
                                titleClassName='activity-list-title'
                                className='activity-list-in-map'
                                onItemClick={setCurrentActivity}
                                navItems={activitiesMenuItems}
                            />
                        </> :
                        <div style={{ position: 'absolute', top: '50%', left: '50%' }}>
                            <CircularProgress size={'20vh'} style={{ position: 'absolute', top: '50%' }} />
                        </div>
                }
            </div>
            <Snackbar
                open={errorOpen}
                autoHideDuration={3000}
                onClose={handleSnackbarClose}>
                <Alert severity='warning' dir='rtl'>שגיאה בהוצאת המיקום של ההתנדבות.</Alert>
            </Snackbar>

        </>
    );
};

export default ActivitiesPage;