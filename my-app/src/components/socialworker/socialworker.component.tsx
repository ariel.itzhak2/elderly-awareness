import Profile from './profile/profile.component'
import React, { useState, useEffect } from 'react';
import NavBar from '../verticalList/verticalList.component';
import NavItem from '../../interfaces/navItemInterface';
import { getAllElderlyNames } from '../../mocks/elderlyMock'
import './socialworker.scss'
import { ThemeProvider } from '@mui/system';
import { createTheme } from '@mui/material';
import { Person } from '../../interfaces/personInterface';
import { useDispatch } from 'react-redux';
import { CurrentElderAction, CurrentElderActionTypes } from '../../redux/reducers/currentElderReducer';
import { AllEldersAction, EldersActionTypes } from '../../redux/reducers/allEldersReducer';
import { Elder } from '../../interfaces/elderInterface';
import Header from '../header/header.component';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers/rootReducer';
import { stat } from 'fs';

const SocialWorkerPage = () => {

  const dispatch = useDispatch();

  const theme = createTheme({
    typography: {
      fontFamily: "'Alef', sans-serif, Arial"
    }
  });

  const onElderClick = (clickedItem: NavItem<Person>) => {
    const action: CurrentElderAction = {
      type: CurrentElderActionTypes.setCurrentElder,
      personId: clickedItem.obj._id
    };
    dispatch(action);
  }

  const allElderlyNames = useSelector((state : RootState) => state.allElders);


  useEffect(() => {
    // fetch all elderly
    const newAllElderlyNames = getAllElderlyNames();
    const action: AllEldersAction = {
      type: EldersActionTypes.setElders,
      elders: newAllElderlyNames.map(elder => elder.obj)
    };
    dispatch(action);
    onElderClick(newAllElderlyNames[0]);  // Auto press the first elder;
  }, []);

  const url = "https://thumbs.dreamstime.com/b/indonesian-old-man-face-potrait-palembang-city-his-wrinkles-skin-white-hair-indonesia-august-193194107.jpg"
  return (
    <ThemeProvider theme={theme}>
      <Header userPhoto={url} />
      <div className="SocialWorkerPage">
        <NavBar className='SocialWorkerNavBar'
          navItems={allElderlyNames.map((elder) => ({
            obj: elder,
            link: elder._id.toString(),
            text: elder.first_name + " " + elder.last_name  
          }))} onItemClick={(clickedItem) => onElderClick(clickedItem)} />
        <Profile />
      </div>
    </ThemeProvider>

  );
};

export default SocialWorkerPage;