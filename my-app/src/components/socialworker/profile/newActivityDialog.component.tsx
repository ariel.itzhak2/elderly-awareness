import React, { useState } from 'react';
import { Alert, Button, Dialog, Paper, Snackbar, Stack, TextField, Typography } from "@mui/material";

interface NewActivityDialogProps {
    open: boolean;
    onClose: () => void;
}

const NewActivityDialog = ({ open, onClose }: NewActivityDialogProps) => {
    const handleClose = () => {
        onClose();
    }

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [address, setAddress] = useState('');

    const [successOpen, setSuccessOpen] = useState(false);
    const [errorOpen, setErrorOpen] = useState(false);

    const handleSend = () => {
        if (title.length <= 0 ||
            description.length <= 0 ||
            address.length <= 0) {
            setErrorOpen(true);
            return;
        }
        console.log(`Sent ${title}, ${description}, ${address}`);
        setSuccessOpen(true);
        setTitle('');
        setDescription('');
        setAddress('');
        onClose();
    }

    const handleSnackbarClose = () => {
        setSuccessOpen(false);
        setErrorOpen(false);
    }

    return (
        <div>
            <Dialog onClose={handleClose} open={open} fullWidth maxWidth="md" >
                <Paper variant="outlined"
                    dir="rtl"
                    className='NewActivityDialogPaper'>
                    <div style={{ padding: "3%" }}>
                        <Typography variant="h4">יצירת התנדבות חדשה</Typography>
                        <hr />
                        <Stack
                            direction="column"
                            justifyContent="flex-start"
                            alignItems="flex-start"
                            spacing={2}
                        >
                            <Stack
                                direction="row" justifyContent="flex-start" alignItems="center" spacing={2} sx={{ width: "100%" }}
                            >
                                <Typography sx={{ margin: 1 }} variant='h5'>כותרת:</Typography>
                                <TextField fullWidth onChange={e => setTitle(e.target.value)} defaultValue={title}></TextField>
                            </Stack>
                            <Stack
                                direction="row" justifyContent="flex-start" alignItems="center" spacing={2} sx={{ width: "100%" }}
                            >
                                <Typography sx={{ margin: 1 }} variant='h5'>תיאור כללי:</Typography>
                                <TextField fullWidth multiline onChange={e => setDescription(e.target.value)} defaultValue={description}></TextField>
                            </Stack>
                            <Stack
                                direction="row" justifyContent="flex-start" alignItems="center" spacing={2} sx={{ width: "100%" }}
                            >
                                <Typography sx={{ margin: 1 }} variant='h5'>מיקום:</Typography>
                                <TextField fullWidth onChange={e => setAddress(e.target.value)} defaultValue={address}></TextField>
                            </Stack>
                            <Stack
                                direction="row" justifyContent="center" alignItems="center" sx={{ width: "100%" }}>
                                <Button variant='contained' onClick={handleSend}>שלח</Button>
                            </Stack>
                        </Stack>
                    </div>
                </Paper>
            </Dialog>
            <Snackbar
                open={successOpen}
                autoHideDuration={3000}
                onClose={handleSnackbarClose}>
                <Alert severity='success' dir='rtl'>ההתנדבות הוזנה בהצלחה!</Alert>
            </Snackbar>
            <Snackbar
                open={errorOpen}
                autoHideDuration={3000}
                onClose={handleSnackbarClose}>
                <Alert severity='error' dir='rtl'>תקלה - ישנם שדות חסרים!</Alert>
            </Snackbar>
        </div>
    )

}

export default NewActivityDialog;