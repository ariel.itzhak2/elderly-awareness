import { Avatar, Dialog, makeStyles } from "@mui/material";
import React, { useState } from 'react';

export interface SimpleDialogProps {
    open: boolean;
    onClose: () => void;
    alt: string;
    src: string;
}

const ProfileAvatarDialog = (props: SimpleDialogProps) => {
    const { onClose, open, alt, src } = props;
    const handleClose = () => {
        onClose();
    };

    return (
        <Dialog onClose={handleClose} open={open}>
            <img onClick={handleClose}
                alt={alt}
                src={src}
                style={{ maxWidth: "100%", height: "min(auto, 85%)" }}
            />
        </Dialog>
    )
}

const ProfileAvatar = (props: any) => {
    const [dialogOpen, setDialogOpen] = useState(false);
    const handleClickOpen = () => {
        setDialogOpen(true);
    }
    const handleClose = () => {
        setDialogOpen(false);
    }

    return (
        <div>
            <Avatar onClick={handleClickOpen}
                alt={props.alt}
                src={props.src}
                sx={{ width: 250, height: 250, boxShadow: 20 }}
            />
            <ProfileAvatarDialog open={dialogOpen}
                onClose={handleClose}
                src={props.src}
                alt={props.alt} />
        </div>
    )
}

export default ProfileAvatar;