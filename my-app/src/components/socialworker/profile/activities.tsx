export enum ActivityStatus {
    Pending = 0,
    Resolved = 1,
}

export class Location {
    lat: Number;
    lng: Number;

    constructor(lat: Number, lng: Number) {
        this.lat = lat;
        this.lng = lng;
    }
}

export class Activity {
    activityStatus: ActivityStatus;
    activityId: Number;
    title: string;
    description: string;
    category: string;
    address: string;
    location: Location;
    elderId: Number;

    constructor(
        activityStatus: ActivityStatus,
        activityId: Number,
        title: string,
        description: string,
        category: string,
        address: string,
        location: Location,
        elderId: Number
    ) {
        this.activityStatus = activityStatus;
        this.activityId = activityId;
        this.title = title;
        this.description = description;
        this.category = category;
        this.address = address;
        this.location = location;
        this.elderId = elderId;
    }
}