import React, { useState } from 'react';
import { Button, Paper } from "@mui/material";
import './profile.component.scss';
import { Stack } from '@mui/system';
import ProfileAvatar from "./profileAvatar.component"
import ProfileContent from "./profileContent.component"
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism';
import NewActivityDialog from './newActivityDialog.component';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux/reducers/rootReducer';

const Profile = () => {// TODO: Replace image with backend info

    const allElders = useSelector((state: RootState) => state.allElders);
    const currentElder = useSelector((state: RootState) => state.currentElder);
    const elder = allElders.find(elder => elder._id === currentElder);

    const [dialogOpen, setDialogOpen] = useState(false);
    const handleClickOpen = () => {
        setDialogOpen(true);
    }
    const handleClose = () => {
        setDialogOpen(false);
    }
    return (
        <div style={{ margin: "1vh", width: "75vw" }}>
            <Paper elevation={6}
                className={"ProfilePaper"}
            >
                <Stack
                    direction="column"
                    justifyContent="flex-start"
                    alignItems="space-around"
                    spacing={2}
                    sx={{ padding: "7%" }}>
                    <ProfileAvatar src={elder?.photo} />
                    <Button
                        onClick={handleClickOpen}
                        variant="contained"
                        startIcon={<VolunteerActivismIcon />}
                    >
                        יצירת התנדבות חדשה
                    </Button>
                </Stack>
                <NewActivityDialog open={dialogOpen}
                    onClose={handleClose} />
                <ProfileContent elder={elder} />
            </Paper>
        </div>

    )
}

export default Profile;