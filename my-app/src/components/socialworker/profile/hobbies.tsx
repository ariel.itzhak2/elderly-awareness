import React from 'react';
import AddShoppingCartRoundedIcon from '@mui/icons-material/AddShoppingCartRounded';
import AddAPhotoRoundedIcon from '@mui/icons-material/AddAPhotoRounded';
import AgricultureRoundedIcon from '@mui/icons-material/AgricultureRounded';
import ForestRoundedIcon from '@mui/icons-material/ForestRounded';

export enum Hobbies {
    SHOPPING = 1,
    PHOTOGRAPHY = 2,
    FARMING = 3,
    PARK = 4,

}

export function getHobbyIcon(hobby: Hobbies) {
    switch(hobby) {
        case Hobbies.SHOPPING: return <AddShoppingCartRoundedIcon />
        case Hobbies.PHOTOGRAPHY: return <AddAPhotoRoundedIcon />
        case Hobbies.FARMING: return <AgricultureRoundedIcon />
        case Hobbies.PARK: return <ForestRoundedIcon />
    }
}

export function getHobbyHebrew(hobby: Hobbies) {
    switch (hobby) {
        case Hobbies.SHOPPING: return "קניות"
        case Hobbies.PHOTOGRAPHY: return "צילום"
        case Hobbies.FARMING: return "חקלאות"
        case Hobbies.PARK: return "גינות ופארקים"
    }
}