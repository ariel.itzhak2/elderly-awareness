import React, { useState } from 'react';
import { Stack } from "@mui/system";
import { Hobbies, getHobbyIcon, getHobbyHebrew } from './hobbies'
import { Activity, ActivityStatus, Location } from './activities';
import { Box, Collapse, createTheme, Divider, Grid, List, ListItem, ListItemButton, ListItemIcon, ListItemText, ThemeProvider, Typography } from '@mui/material';
import HelpIcon from '@mui/icons-material/Help';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux/reducers/rootReducer';
import { Elder } from '../../../interfaces/elderInterface';

const theme = createTheme({
    typography: {
        fontFamily: "'Alef', sans-serif, Arial"
    }
});

const getHobbyList = (hobbies: Hobbies[]) => {
    const style = {
        width: "50%",
        bgcolor: 'background.paper',
    };
    const typographyStyle = {
        typography: {
            fontSize: 200
        }
    }
    return (
        <ThemeProvider theme={typographyStyle}>
            <List sx={style}
                component="nav"
                aria-label='תחביבים'>
                {hobbies.map(hobby => (
                    <div>
                        <ListItem disablePadding
                            sx={{}}>
                            <ListItemIcon>
                                {getHobbyIcon(hobby)}
                            </ListItemIcon>
                            <ListItemText primary={
                                <Typography>{getHobbyHebrew(hobby)}</Typography>
                            } />
                        </ListItem>
                        <Divider />
                    </div>
                ))}
            </List>
        </ThemeProvider>

    )
}

const getActivitiesList = () => {
    let activities = [
        new Activity(ActivityStatus.Pending, 1, "קניות",
            "עזרה בקניות בסופר בימי שלישי", "הקטגוריה שלי",
            "רמת השרון", new Location(34, 34), 1),
        new Activity(ActivityStatus.Resolved, 2, "ישיבה בפארק",
            "ישיבה בפארק בימי רביעי בבוקר להאכיל ציפורים", "הקטגוריה שלי",
            "תל אביב", new Location(34, 34), 1),
    ]
    return (
        <List
            component="nav"
        >
            {activities.map(activity => {
                const [open, setOpen] = useState(false);
                const handleClick = () => {
                    setOpen(!open);
                };
                const activityIcon = activity.activityStatus === ActivityStatus.Pending ?
                    <HelpIcon style={{margin: 3}}/> :
                    <CheckCircleIcon color="success" />
                const activityStatusText = activity.activityStatus === ActivityStatus.Resolved ?
                    "נמצא מתנדב" : "עדיין אין מתנדב";

                return (
                    <div>
                        <ListItemButton onClick={handleClick}>
                            <ListItemIcon>
                                {activityIcon}
                            </ListItemIcon>
                            <ListItemText primary={
                                <Typography>{activity.title}</Typography>
                            } />
                            {open ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Stack
                                direction="column"
                                justifyContent="flex-start"
                                alignItems="flex-start"
                                spacing={2}
                            >
                                <Typography>תיאור: {activity.description}</Typography>
                                <Stack direction="row"
                                    alignItems="center">
                                    <Typography>סטטוס:  </Typography>
                                    {activityIcon}
                                    <Typography>{activityStatusText}</Typography>
                                </Stack>

                                <Typography>קטגוריה: {activity.category}</Typography>
                                <Typography>כתובת: {activity.address}</Typography>
                            </Stack>
                        </Collapse>
                    </div>
                )
            })}
        </List>
    )
}

interface Props {
    elder : Elder
}

const ProfileContent : React.FC<Props> = ({ elder }) => {
    // let name = "ארד דוד", age = 21, address = "סמ חצב 1 רמת השרון", phone = "0587434848";

    let hobbies = [
        Hobbies.PARK, Hobbies.SHOPPING
    ];

    return (
        <ThemeProvider theme={theme}>
            <Stack
                direction="column"
                justifyContent="flex-start"
                alignItems="flex-start"
                spacing={2}
                dir="rtl"
                id="ProfileMainPane"
            >
                <Typography variant="h3">{elder?.first_name} {elder?.last_name}</Typography>
                <Typography variant="h4">גיל: {elder?.birth_date.toDateString()}</Typography>
                <Typography variant="h4">כתובת מגורים: {elder?.address}</Typography>
                <Typography variant="h4">טלפון: {elder?.phone_number}</Typography>
                <br />
                <Typography variant='h5'>תחביבים:</Typography>
                {getHobbyList(hobbies)}
                <br />
                <Typography variant='h5'>התנדבויות פעילות:</Typography>
                <div style={{ width: "100%" }}>
                    {getActivitiesList()}
                </div>
            </Stack>
        </ThemeProvider>
    )
}

export default ProfileContent;