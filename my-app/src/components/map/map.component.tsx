import Leaflet, { latLngBounds, LatLngLiteral } from "leaflet";
import React, { useState } from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import ActivityDetailsPopup from "./activityDetails/activityDetails.component";
import { Activity } from "../../interfaces/activityInterface";
import "leaflet/dist/leaflet.css";
import './map.component.scss';

type MapProps = {
  markers?: Activity[];
  mapRef?: React.Ref<Leaflet.Map>;
}

const icon = Leaflet.icon({
  iconUrl: '/images/marker-icon.png',
  iconAnchor: [13, 40]
});

const LeafletMap = ({ markers = [], mapRef }: MapProps) => {
  markers = markers.filter(marker => marker.location !== null);
  const bounds = latLngBounds(markers.map(markers => markers.location));
  return (
    <>
      {
        markers?.length ?
          <MapContainer
            ref={mapRef}
            // center={markers[0]?.location}
            // zoom={13}
            bounds={bounds}
            style={{ height: '92.5vh' }}
            scrollWheelZoom>
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution="&copy; Veteran Team"
            />
            {
              markers.map(marker => (
                <Marker
                  position={marker.location}
                  icon={icon}>
                  <Popup>
                    <ActivityDetailsPopup
                      {...marker}
                    />
                  </Popup>
                </Marker>
              ))
            }
          </MapContainer> : null
      }
    </>
  );
};

export default LeafletMap;