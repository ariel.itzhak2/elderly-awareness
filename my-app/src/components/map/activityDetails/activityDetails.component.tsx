import React, { useState } from "react";
import { Activity } from "../../../interfaces/activityInterface";
import './activityDetails.component.scss';

const ActivityDetailsPopup: React.FC<Activity> = ({ elderName, address, description, title }) => {

    const regularFields: {fieldName: string, fieldValue: string}[] = [
        {
            fieldName: 'כתובת',
            fieldValue: address
        },
        {
            fieldName: 'תיאור',
            fieldValue: description,
        }
    ];

    return (
        <div className="activity-details-popup">
            <div className="activity-details-title">
                {title}
            </div>
            {
                regularFields.map(field => (
                    <div key={field.fieldName} className="activity-details-field">
                        <b>{field.fieldName} - </b>
                        {field.fieldValue}
                    </div>
                ))
            }
            <div 
                className="activity-submition-button">
               הגשת מועמדות להתנדבות 
            </div>
        </div>
    )
}

export default ActivityDetailsPopup;