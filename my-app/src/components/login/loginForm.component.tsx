import { Avatar, Card, CardActionArea, Paper, TextField, Typography } from "@mui/material";
import { Stack } from "@mui/system";
import "./login.component.scss";
import LoginInputForm from './loginInputForm.component';

const LoginForm = () => {
    return (
        <Card id="loginForm" sx={{ maxWidth: 345 }}>
            <Stack direction="column"
                justifyContent="flex-start"
                alignItems="center"
                spacing={2}
                className="loginCard"
            >
                <br />
                <Avatar alt="logo"
                    src="logo.jpg"
                    sx={{ width: 100, height: 100 }}
                />
                <Typography variant="h4" style={{ color: 'white' }}>התחברות</Typography>
                <LoginInputForm />
            </Stack>
        </Card>
    );
};

export default LoginForm;