import { Button, TextField } from '@mui/material';
import { Stack } from '@mui/system';
import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import "./login.component.scss";

const ENTER_CODE = 13;

const LoginInputForm = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const doLogin = (username: string, password: string) => {
        if (username.length <= 0 || password.length <= 0) {
            return;
        }
        localStorage.setItem('username', username);
        if (username === 'admin') {
            navigate('/worker');
        } else {
            navigate('/');
        }
    }

    const handleKeyDown = (event: any) => {
        if (event.keyCode === ENTER_CODE) {
            doLogin(username, password);
        }
    }

    return (
        <Stack direction="column"
            justifyContent="flex-start"
            alignItems="center"
            spacing={2}
            className="loginCard"
        >
            <TextField label="שם משתמש"
                variant="filled"
                InputLabelProps={{ style: { color: "white" } }}
                sx={{ input: { color: "white" } }}
                onChange={e => setUsername(e.target.value)}
                onKeyDown={handleKeyDown}
            />
            <TextField label="סיסמה" type="password" variant="filled"
                InputLabelProps={{ style: { color: "white" } }}
                sx={{ input: { color: "white" } }}
                onChange={e => setPassword(e.target.value)}
                onKeyDown={handleKeyDown}
            />
            <Button variant="contained"
                onClick={() => doLogin(username, password)}>
                התחבר
            </Button>
            <Button variant="contained">הרשמה</Button>
        </Stack>
    );
}

export default LoginInputForm;