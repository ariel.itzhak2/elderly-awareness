import React, { useState, useRef, useEffect } from 'react';
import './header.component.scss';
import { ReactComponent as NotificationIcon } from './notification-bell.svg'
import NotificationList from '../notificationList/notificationList.component'
import { Stack } from '@mui/system';
import { Button, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';

interface Props {
  userPhoto: string;
}

const Header: React.FC<Props> = ({ userPhoto }) => {

  const [anchorEl, setAnchorEl] = useState(null);
  const svgRef = useRef(null);
  const navigate = useNavigate();

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (svgRef.current && !svgRef.current.contains(event.target)) {
        handleClose();
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [svgRef]);

  const handleDropdownClick = (event: any) => {
    setAnchorEl(svgRef.current);
  }

  const handleClose = () => {
    setAnchorEl(null);
  }

  const handleLogout = () => {
    localStorage.removeItem('username');
    navigate('/login');
  }

  let userName = localStorage.getItem('username');
  
  return (
    <header className='header' >
      <div style={{ width: "85%" }}>
        <Button variant='contained' onClick={handleLogout}>התנתק</Button>
      </div>
      <NotificationIcon className={`icon`}
        ref={svgRef} onClick={handleDropdownClick} />
      <NotificationList anchorEl={anchorEl} handleClose={handleClose} />
      <div dir="rtl" style={{margin: 4}}>
        <Typography fontSize={20}>שלום, {userName}!</Typography>
      </div>
      <img src={userPhoto} alt={userName} className="user-photo" />
    </header>
  );
};

export default Header;