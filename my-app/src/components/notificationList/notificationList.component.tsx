import React, { useState, useEffect } from 'react';
import { fetchNotifications } from '../../mocks/volunteerRequestsMock'
import NotificationItem from '../../interfaces/volunteerRequestInterface'
import './notificationList.component.scss';
import { Menu, MenuItem } from '@mui/material';


interface Props {
    anchorEl: any;
    handleClose: any;
}

const NotificationList: React.FC<Props> = ({ anchorEl, handleClose }) => {

    const [dropdownItems, setDropdownItems] = useState<NotificationItem[]>([]);

    useEffect(() => {
        // fetch all notifications
        setDropdownItems(fetchNotifications);
    }, [])

    return (
        <Menu anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            // style={{top: "5%"}}
            >
            {dropdownItems.map(item => (
                <MenuItem key={item.elderlyName} onClick={handleClose}>
                    <div className='field'>{item.activityName}</div>
                    <div className='field'>{item.elderlyName}</div>
                    <div className='field'>{item.volunteerFullName}</div>
                </MenuItem>
            ))}
        </Menu>
    );
};



export default NotificationList;