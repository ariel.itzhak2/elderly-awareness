import { Activity, ActivityToCreate } from "../interfaces/activityInterface";
import { communicator } from "./communicator";

class ActivitiesCommunicator {

    private activitiesRoute = `http://${window.location.hostname}:8000/activity/`;

    getAllActivities = async (): Promise<Activity[]> => {
        try {
            const bla = await communicator.get<Activity[]>(this.activitiesRoute);
            console.log(bla);
            return bla.map(activity => ({
                ...activity,
                location: activity.location
            }));
        } catch {
            return null;
        }
    }

    createNewActivity = async (activityToCreate: ActivityToCreate) => {
        return communicator.post(this.activitiesRoute, activityToCreate);
    }
}

export const activitiesCommunicator = new ActivitiesCommunicator();