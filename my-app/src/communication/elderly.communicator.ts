import { Activity, ActivityToCreate } from "../interfaces/activityInterface";
import { Elder } from "../interfaces/elderInterface";
import { communicator } from "./communicator";

class ActivitiesCommunicator {

    private eldersRoute = 'http://localhost:8000/elder/';

    getAllElders = async (): Promise<Elder[]> => {
        try {
            const bla = await communicator.get<Elder[]>(this.eldersRoute);
            return bla.map(elder => ({
                ...elder
            }));
        } catch {
            return null;
        }
    }

    createNewActivity = async (activityToCreate: ActivityToCreate) => {
        return communicator.post(this.eldersRoute, activityToCreate);
    }
}

export const activitiesCommunicator = new ActivitiesCommunicator();