import axios from 'axios';

class Communicator {

    public post = async <T = any>(url: string, body: any): Promise<T> => {
        const result = await axios.post(url, body);
        return result.data as T;
    }

    public get = async <T = any>(url: string): Promise<T> => {
        try {
            const result = await axios.get(url);
            return result.data as T;
        } catch (e) {
            console.log('hola error')
        }
    }

    public put = async <T = any>(url: string, body: any): Promise<T> => {
        const result = await axios.put(url, body);
        return result.data as T;
    }
}

export const communicator = new Communicator()