export default interface NavItem {
    volunteerFullName: string;
    activityName: string;
    elderlyName: string;
  }