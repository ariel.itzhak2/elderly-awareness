export type PersonId = Number;

export interface Person {
    _id: PersonId;
    first_name: string;
    last_name: string;
    photo: string;
    address: string;
    desc: string;
    phone_number: string;
    birth_date: Date;
}