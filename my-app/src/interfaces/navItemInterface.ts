export default interface NavItem<T = any> {
    obj: T;
    text: string;
    link: string;
  }