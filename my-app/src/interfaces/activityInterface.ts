import { LatLngLiteral } from "leaflet";

export type ActivityId = string;

export enum ActivityStatus {
    PENDNING = 0,
    RESOLVED = 1,
}

export interface Activity {
    activityId: ActivityId;
    status: ActivityStatus;
    title: string;
    description: string;
    location: LatLngLiteral;
    address: string;
    elderName: string;
    submittions: string[];
}

export type ActivityToCreate = Pick<Activity, 'title' | 'description' | 'address'>;