export enum SubmissionStatus {
    PENDING = "pending",
    APPROVED = "approved",
    REJECTED = "rejected",
}

export interface Submission {
    submissionId: string;
    volunteerId: string;
    status: SubmissionStatus;
}