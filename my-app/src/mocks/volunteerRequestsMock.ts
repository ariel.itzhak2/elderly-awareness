import NotificationItem from '../interfaces/volunteerRequestInterface'

export const fetchNotifications = (): NotificationItem[]  => {
    return [
        {
            activityName: "טיול בפארק",
            elderlyName: "שולה זקן",
            volunteerFullName: "ארד צעירי"
        },
        {
            activityName: "האכלת יונים",
            elderlyName: "דיא סבא",
            volunteerFullName: "בן קטן"
        }
    ]
}