import { Activity, ActivityStatus } from "../interfaces/activityInterface";

export const activitiesMock: Activity[] = [
    {
        activityId: '1',
        address: 'הרצליה',
        description: 'מגניב בטירוף',
        elderName: 'שם מגניב',
        location: {
            lat: 32.164860,
            lng: 34.844170,
        },
        title: 'האכלת יונים',
        status: ActivityStatus.PENDNING,
        submittions: [],
    },
    {
        activityId: '2',
        address: 'הרצליה',
        description: 'קול קול',
        elderName: 'שולה זקן',
        location: {
            lat: 32.146461,
            lng: 34.804619,
        },
        title: 'דברים',
        status: ActivityStatus.PENDNING,
        submittions: [],
    }
]