import NavItem from '../interfaces/navItemInterface'
import { Person } from '../interfaces/personInterface'

export const getAllElderlyNames = (): NavItem<Person>[] => {
    return [
        {
            link: "",
            text: "שולה זקן",
            obj: {
                _id: 1,
                first_name: "שולה",
                last_name: "זקן",
                photo: "https://images.maariv.co.il/image/upload/f_auto,fl_lossy/c_fill,g_faces:center,h_792,w_900/607070",
                address: "כפר סבא",
                desc: "אחלה סבתא כמו שכולם חושבים",
                phone_number: "0505454545",
                birth_date: new Date("04.10.1999")
            }
        },
        {
            link: "",
            text: "דיא סבא",
            obj: {
                _id: 2,
                first_name: "דיא",
                last_name: "סבא",
                photo: "https://www.israelhayom.co.il/wp-content/uploads/2023/01/29/29/86016_UDIZ0083a_2020-02-18-e1675008375104.jpg",
                address: "כפר יונה",
                desc: "יש לו מלא נכדים וגולים",
                phone_number: "0505454545",
                birth_date: new Date("04.10.1999")
            }
        },
        {
            link: "",
            text: "אדם אליעזרוהגזרוב",
            obj: {
                _id: 3,
                first_name: "אדם",
                last_name: "אליעזרוהגזרוב",
                photo: "https://i.stack.imgur.com/Zo5at.jpg?s=192&g=1",
                address: "רמת גן",
                desc: "הוא סתם צעיר בתכלס",
                phone_number: "0505454545",
                birth_date: new Date("04.10.1999")
            }
        }
    ]
}